import axios from 'axios';


export const ENDPOINT = 'http://localhost:8000';

export function fetchClients (){
    return axios.get(`${ENDPOINT}/clients`);
}