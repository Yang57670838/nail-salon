import React, { Component } from 'react';
import './App.css';
import * as API from '../api/clients/getClients';

class App extends Component {

  state = {
    clients: []
  }

  componentDidMount() {
    API.fetchClients()
      .then(response => {
        this.setState({
          clients: response.data
        })
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    return (
      <div className="App">
        <h1>Nail Salon</h1>
      </div>
    );
  }
}

export default App;
