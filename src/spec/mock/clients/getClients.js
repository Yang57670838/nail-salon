const getMockClients = () => {
    return [
      {
        id: 'FAEED479-0D60-423D-BBCB-2E94537E46FB',
        fullName: 'Anita Maeda',
        fname: 'Anita',
        lname: 'Maeda',
        registerDate: '2018-01-01',
        lastVisitDate: '2019-02-02',
        gender: 'female'
      },
      {
        id: 'CAF41292-1E68-4F2D-A615-4F40AF3DA97E',
        person: 'Ann Tiffany',
        fname: 'Ann',
        lname: 'Tiffany',
        registerDate: '2018-01-01',
        lastVisitDate: '2019-02-02',
        gender: 'female'
      }
    ];
  };
  
  export default getMockClients;